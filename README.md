## create thread-pool

```
[dependencies]
th_pool = "*"
```



```rust
fn main() {
    run();
}

fn run() {
    use th_pool::Pool;
    fn thread_owner_optimization() {
        use std::cell::RefCell;
        use std::collections::HashSet;
        use std::sync::Arc;

        let pool: Arc<Pool<RefCell<_>>> =
            Arc::new(Pool::new(Box::new(|| RefCell::new(HashSet::new()))));
        pool.get().value().borrow_mut().insert("thead".to_string());
        println!("pool::{:?}", pool.get().value().borrow());

        let pool1 = pool.clone();
        let t1 = std::thread::spawn(move || {
            let guard = pool1.get();
            let v = guard.value();
            v.borrow_mut().insert("thead".to_string());
            println!("thead_1::{:?}", v.borrow());
        });

        let pool2 = pool.clone();
        let t2 = std::thread::spawn(move || {
            let guard = pool2.get();
            let v = guard.value();
            v.borrow_mut().insert("thead".to_string());
            println!("thead_2::{:?}", v.borrow());
        });

        let pool3 = pool.clone();
        let t3 = std::thread::spawn(move || {
            let guard = pool3.get();
            let v = guard.value();
            v.borrow_mut().insert("thead".to_string());
            println!("thead_3::{:?}", v.borrow());
        });

        t1.join().unwrap();
        t2.join().unwrap();
        t3.join().unwrap();


    
    }
    thread_owner_optimization();
}
```