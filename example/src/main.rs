fn main() {
    run();
}

fn run() {
    fn thread_owner_optimization() {
        use std::cell::RefCell;
        use std::collections::HashSet;
        use std::sync::Arc;
        use th_pool::Pool;

        let pool: Arc<Pool<RefCell<_>>> =
            Arc::new(Pool::new(Box::new(|| RefCell::new(HashSet::new()))));
        pool.get().value().borrow_mut().insert("thead".to_string());
        println!("pool::{:?}", pool.get().value().borrow());

        let pool1 = pool.clone();
        let t1 = std::thread::spawn(move || {
            let guard = pool1.get();
            let v = guard.value();
            v.borrow_mut().insert("thead".to_string());
            println!("thead_1::{:?}", v.borrow());
        });

        let pool2 = pool.clone();
        let t2 = std::thread::spawn(move || {
            let guard = pool2.get();
            let v = guard.value();
            v.borrow_mut().insert("thead".to_string());
            println!("thead_2::{:?}", v.borrow());
        });

        let pool3 = pool.clone();
        let t3 = std::thread::spawn(move || {
            let guard = pool3.get();
            let v = guard.value();
            v.borrow_mut().insert("thead".to_string());
            println!("thead_3::{:?}", v.borrow());
        });

        t1.join().unwrap();
        t2.join().unwrap();
        t3.join().unwrap();

        // If we didn't implement the single owner optimization, then one of
        // the threads above is likely to have mutated the [a, x] vec that
        // we stuffed in the pool before spawning the threads. But since
        // neither thread was first to access the pool, and because of the
        // optimization, we should be guaranteed that neither thread mutates
        // the special owned pool value.

        // assert_eq!(
        //     VecDeque::from(vec!["Andrew".to_string()]),
        //     *pool.get().value().borrow()
        // );
    }
    thread_owner_optimization();
    fn pool_() {
        use std::cell::RefCell;
        use std::sync::Arc;
        use th_pool::Pool;

        let pool: Arc<Pool<RefCell<_>>> = Arc::new(Pool::new(Box::new(|| RefCell::new(vec![]))));
    }
}
